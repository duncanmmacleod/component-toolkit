# Components Toolkit

Gitlab provides a toolkit you can use in your project's pipeline to help you during the development process of components.

[[_TOC_]]

## Usage

### All jobs

You can include all the below mentioned components by including a single component.

```yml
include:
  - component: gitlab.com/$CI_PROJECT_PATH/all-jobs@$CI_COMMIT_SHA
    inputs:
      stage: 'prepare'
      license: 'mit'
```

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `stage` | 'test'      | The pipeline stage header where this job will be run. |
| `license` | 'mit'         | The license name that will be checked against the component project. (License name comparison is case sensitive.) |

### Readme check

Check README.md file with `Usage` headers and an optional `Contributing` header. Since these headers are required, you can automate these checks with a single component.

It is part of a `all-jobs` components but can be included individually too.

```yml
include:
  - component: gitlab.com/$CI_PROJECT_PATH/readme-check@$CI_COMMIT_SHA
    inputs:
      stage: 'prepare'
```

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `stage` | 'test'      | The pipeline stage header where this job will be run. |

### License check

Check LICENSE.md file, whether the given component has a valid LICENSE file.

This job uses a GitLab API to fetch project license to check against the provided input. For more information on the terms of various licenses, see [this site](https://choosealicense.com/) or any of the many other resources available online.

It is part of `all-jobs` components but can be included individually too.

```yml
include:
  - component: gitlab.com/$CI_PROJECT_PATH/license-check@$CI_COMMIT_SHA
    inputs:
      license: "mit"
      stage: pre-test
```

| Input     | Default value | Description |
| --------- | ------------- | ----------- |
| `stage`   | 'test'        | The pipeline stage header where this job will be run. |
| `license` | 'mit'         | The license name that will be checked against the component project. (License name comparison is case sensitive.) The license value is picked from [GitLab project API](https://docs.gitlab.com/ee/api/projects.html#get-single-project). So make sure it is the same as returned in the API response. |

### Ensure job added check

When you create a component, and want to test whether it adds the desired job in the pipeline, you can verify that using this check. Instead of making the API call and checking it yourself, this component handles the required check.

It is part of `all-jobs` components but it can be included individually too.

You can include this job multiple times if your component adds multiple jobs.

The `job_name` input could be a common string across multiple jobs. It will match across all the jobs, for example, `job_name: test` would match various jobs like `test`, `test-1`, `testing`, etc.

```yml
include:
  - component: gitlab.com/$CI_PROJECT_PATH/ensure-job-added@$CI_COMMIT_SHA
    inputs:
      stage: test
      job_name: test_component
```

| Input | Default value | Description |
| ----- | ------------- | ----------- |
| `stage` | 'test'      | The pipeline stage where this job will be run. |
| `job_name` | 'test_component' | The name of the job to match and compare against that will be added by the component. |

## Contributing

Please read about CI/CD components and best practices at: https://docs.gitlab.com/ee/ci/components
